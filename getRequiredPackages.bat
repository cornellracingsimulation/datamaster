:: Before this can be run the following need to be installed
:: 	MATLAB: https://www.mathworks.com/
:: 	Python: https://www.python.org/
:: 	pip: 	https://pip.pypa.io/
:: Close all open Python and MATLAB windows before running

::Google Python API
pip install --upgrade google-api-python-client

::Python Keyring
pip install keyring